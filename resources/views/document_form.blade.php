@extends('layout')

@section('content')
<form action="{{ route('document_upload') }}" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
<input type="file" name="document">
<input type="submit" value="Įkelti failą">
</form>
@endsection